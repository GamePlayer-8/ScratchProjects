# scratch-projects
Some old Scratch projects.

## Lore
Some years ago I had account @ScratchGry, but I deleted it due to inactivity. There are the projects from that account (yes, they're crappy, but it's my lonely story line where nobody even care, what I'm doing).

## Contributing and patching
Heyo, if you want to report an issue or create a new pull request **please firstly** go to **[GamePlayer Issues Center](https://gameplayer-8.codeberg.page/issues)** or go to **[Github Issues Center](https://github.com/GamePlayer-8/issues/issues)**.

## Questions or contact info
I recommend you going on **[GamePlayer Support Center](https://gameplayer-8.codeberg.page/helpcenter)** or contact with me directly via email **gameplayer2019pl@tutamail.com**.
